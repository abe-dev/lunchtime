package com.abedev.lunchtime.service;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.abedev.lunchtime.Constants;
import com.abedev.lunchtime.Utils;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceEntry;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.RateEntry;
import com.abedev.lunchtime.data.source.remote.firebase.RatesDataSource;
import com.abedev.lunchtime.data.source.remote.foursquare.FourSquareServiceDataSource;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;

/**
 * Created by home on 18.09.2016.
 */
public class RemoteDataService extends IntentService {
    public static final String ACTION_REMOTE_DATA_LOADED = "com.abedev.lunchtime.remotedataloaded";
    private static final long FIREBASE_TIMEOUT = 1000;
    private static final String EXTRA_ACTION_TYPE = "ACTION_TYPE";
    private static final int ACTION_LOAD_BY_LOCATION = 1;
    private static final int ACTION_LOAD_BY_AREA = 2;
    private static final int ACTION_LOAD_BY_ID = 3;
    private static final int ACTION_SET_RATE = 4;
    private static final String EXTRA_LOCATION = "LOCATION";
    private static final String EXTRA_SW = "SW";
    private static final String EXTRA_NE = "NE";
    private static final String EXTRA_ID = "ID";
    private static final String EXTRA_RATE = "RATE";
    private static final String EXTRA_USER = "USER";
    private static final String TAG = RemoteDataService.class.getSimpleName();
    private static final String EXTRA_RADIUS = "RADIUS";
    private FirebaseDatabase firebaseDatabase;

    public RemoteDataService() {
        super(RemoteDataService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        firebaseDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG,"onHandleIntent");
        int actionType = intent.getIntExtra(EXTRA_ACTION_TYPE,-1);
        if (actionType == ACTION_SET_RATE)
            setRate(intent);
        else
            loadData(intent, actionType);
    }

    private void setRate(Intent intent) {
        Log.d(TAG,"setRate");
        RatesDataSource ratesDataSource = getRatesDataSource();
        String placeId = intent.getStringExtra(EXTRA_ID);
        int rate = intent.getIntExtra(EXTRA_RATE,0);
        String user = intent.getStringExtra(EXTRA_USER);
        LatLng location = intent.getParcelableExtra(EXTRA_LOCATION);
        ContentValues cv = new ContentValues();
        cv.put(RateEntry.COLUMN_NAME_ENTRY_ID,placeId);
        cv.put(RateEntry.COLUMN_NAME_USER,user);
        cv.put(RateEntry.COLUMN_NAME_RATE,rate);
        ratesDataSource.storePlaceRate(cv, location);
        getContentResolver().insert(RateEntry.CONTENT_RATE_URI,cv);
        getContentResolver().notifyChange(PlaceWithRateEntry.CONTENT_PLACE_WITH_RATE_URI,null);

    }

    private void loadData(Intent intent, int actionType) {
        Log.d(TAG,"loadData");
        FourSquareServiceDataSource dataSource = getFourSquareDataSource();
        RatesDataSource ratesDataSource = getRatesDataSource();
        List<ContentValues> places;
        List<ContentValues> rates;
        switch (actionType) {
            case ACTION_LOAD_BY_ID:
                places = new ArrayList<>();
                String placeId = intent.getStringExtra(EXTRA_ID);
                places.add(dataSource.getPlaceById(placeId));
                rates = new ArrayList<>();
                ContentValues rate = ratesDataSource.getById(placeId,FIREBASE_TIMEOUT);
                if (rate!=null)
                    rates.add(rate);
                break;
            case ACTION_LOAD_BY_LOCATION:
                LatLng location = intent.getParcelableExtra(EXTRA_LOCATION);
                double radius = intent.getDoubleExtra(EXTRA_RADIUS,Constants.MAX_RADIUS);
                places = dataSource.findPlaces(location, radius);
                rates = ratesDataSource.loadRates(location,radius,FIREBASE_TIMEOUT);
                break;
            case ACTION_LOAD_BY_AREA:
                LatLng sw = intent.getParcelableExtra(EXTRA_SW);
                LatLng ne = intent.getParcelableExtra(EXTRA_NE);
                try {
                    places = dataSource.findPlaces(sw,ne);
                }
                catch (FourSquareServiceDataSource.BoundsTooBigException ex) {
                    LatLng center = Utils.getCenter(sw,ne);
                    try {
                        places = dataSource.findPlaces(center, Constants.MAX_RADIUS);
                    }
                    catch (RuntimeException runEx) {
                        places = null;
                    }
                }
                catch (RuntimeException ex) {
                    places = null;
                }
                if (places == null)
                    rates = null;
                else
                    rates = ratesDataSource.loadRates(sw,ne,FIREBASE_TIMEOUT);
                break;
            default:
                places = null;
                rates = null;
        }


        if (places!=null)
            try {
                updatePlaces(places, rates);
            } catch (RemoteException | OperationApplicationException e) {
                Log.e(TAG,e.getMessage(),e);
            }
        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(new Intent(ACTION_REMOTE_DATA_LOADED));

    }

    private RatesDataSource getRatesDataSource() {
        return new RatesDataSource(firebaseDatabase);
    }

    private void updatePlaces(@NonNull List<ContentValues> places, @Nullable  List<ContentValues> rates)
            throws RemoteException, OperationApplicationException {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        int i = 1;
        for (ContentValues value : places) {
            operations.add(ContentProviderOperation
                    .newInsert(PlaceEntry.CONTENT_PLACE_URI)
                    .withYieldAllowed(i%10==0)
                    .withValues(value)
                    .build());
            i++;
        }
        if (rates!=null)
        for (ContentValues value : rates) {
            operations.add(ContentProviderOperation
                    .newInsert(RateEntry.CONTENT_RATE_URI)
                    .withYieldAllowed(i%10==0)
                    .withValues(value)
                    .build());
            i++;
        }

        getContentResolver().applyBatch(PlacesPersistenceContract.CONTENT_AUTHORITY,operations);
        getContentResolver().notifyChange(PlaceWithRateEntry.CONTENT_PLACE_WITH_RATE_URI,null);

    }

    private FourSquareServiceDataSource getFourSquareDataSource() {
        return new FourSquareServiceDataSource(new OkHttpClient());
    }

    @NonNull
    public static Intent getLoadByLocationIntent(@NonNull LatLng location,
                                                 double radius,
                                                 @NonNull Context context) {
        Intent intent = new Intent(context,RemoteDataService.class);
        intent.putExtra(EXTRA_ACTION_TYPE, ACTION_LOAD_BY_LOCATION);
        intent.putExtra(EXTRA_LOCATION,location);
        intent.putExtra(EXTRA_RADIUS,radius);
        return intent;
    }

    @NonNull
    public static Intent getLoadByAreaIntent(@NonNull LatLng sw,
                                             @NonNull LatLng ne,
                                             @NonNull Context context) {
        Intent intent = new Intent(context,RemoteDataService.class);
        intent.putExtra(EXTRA_ACTION_TYPE, ACTION_LOAD_BY_AREA);
        intent.putExtra(EXTRA_SW,sw);
        intent.putExtra(EXTRA_NE,ne);
        return intent;
    }


    @NonNull
    public static Intent getSetRateIntent(@NonNull String placeId,
                                          @NonNull LatLng location,
                                          @NonNull String user,
                                          int rate,
                                          @NonNull Context context) {
        Intent intent = new Intent(context,RemoteDataService.class);
        intent.putExtra(EXTRA_ACTION_TYPE, ACTION_SET_RATE);
        intent.putExtra(EXTRA_LOCATION,location);
        intent.putExtra(EXTRA_ID,placeId);
        intent.putExtra(EXTRA_USER,user);
        intent.putExtra(EXTRA_RATE,rate);
        return intent;
    }

}
