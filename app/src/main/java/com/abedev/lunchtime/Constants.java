package com.abedev.lunchtime;

/**
 * Created by home on 18.09.2016.
 */
public final class Constants {
    public static final double MAX_RADIUS = 10000;
    public static final double MIN_RADIUS = 500;

    private Constants() {
        throw new UnsupportedOperationException();
    }
}
