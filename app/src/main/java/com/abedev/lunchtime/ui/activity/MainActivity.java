package com.abedev.lunchtime.ui.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.abedev.lunchtime.R;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;
import com.abedev.lunchtime.service.RemoteDataService;
import com.abedev.lunchtime.ui.fragment.DetailFragment;
import com.abedev.lunchtime.ui.fragment.PlacesListFragment;
import com.abedev.lunchtime.ui.fragment.PlacesMapFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        PlaceSelectionListener, DetailFragment.SetRateListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String TAG_FRAGMENT = "fragment";
    private static final String KEY_VIEW_TYPE = "VIEW_TYPE";
    private static final int VIEW_TYPE_MAP = 1;
    private static final int VIEW_TYPE_LIST = 2;
    public static final int REQUEST_LOCATION_PERMISSION = 1000;
    private final LocationRequest LOCATION_REQUEST = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    private int mViewType;
    private boolean mRequestLocationUpdates = false;
    private LocationListener mLocationListener;
    private Location mLastLocation = null;


    public interface TargetLocationListener {
        void onLocationChanged(LatLng location);
    }

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null)
            mViewType = VIEW_TYPE_MAP;
        else
            mViewType = savedInstanceState.getInt(KEY_VIEW_TYPE);
        setContentView(R.layout.activity_main);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        SupportPlaceAutocompleteFragment searchFragment =
                (SupportPlaceAutocompleteFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.searchFragment);
        searchFragment.setOnPlaceSelectedListener(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment, new PlacesMapFragment(), TAG_FRAGMENT)
                    .commitNow();
        }
        mLocationListener = createLocationListaner();
    }


    private LocationListener createLocationListaner() {
        return new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                //just to update last location
                Log.d(TAG, "onLocationChanged location=" + location.toString());
                mLastLocation = location;
            }
        };
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_VIEW_TYPE, mViewType);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result;
        switch (item.getItemId()) {
            case R.id.item_show_type:
                changeShowType();
                result = true;
                break;
            case R.id.item_places_near_me:
                goToLastLocation();
                result = true;
                break;
            default:
                result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    private void changeShowType() {
        if (mViewType == VIEW_TYPE_MAP) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, new PlacesListFragment(), TAG_FRAGMENT)
                    .commitNow();
            mViewType = VIEW_TYPE_LIST;
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, new PlacesMapFragment(), TAG_FRAGMENT)
                    .commitNow();
            mViewType = VIEW_TYPE_MAP;
        }
    }

    @Override
    protected void onResume() {
        mGoogleApiClient.connect();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRequestLocationUpdates) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
            mRequestLocationUpdates = false;
        }
        mGoogleApiClient.disconnect();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void goToLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION_PERMISSION);
            return;
        }
        if (mLastLocation != null)
            locationChanged(mLastLocation);
        else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null)
                locationChanged(location);
        }
        if (!mRequestLocationUpdates) {
            mRequestLocationUpdates = true;
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, LOCATION_REQUEST, mLocationListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                goToLastLocation();
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mRequestLocationUpdates) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, LOCATION_REQUEST, mLocationListener);
        }
    }

    private void locationChanged(Location location) {
        deliverLocationChange(new LatLng(location.getLatitude(),location.getLongitude()));
    }

    @Override
    public void onConnectionSuspended(int i) {
        int f = 1;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        int f = 1;
    }

    @Override
    public void onPlaceSelected(Place place) {
        deliverLocationChange(place.getLatLng());
    }

    @Override
    public void onError(Status status) {

    }

    @Override
    public void onRateSet(ContentValues contentValues, int rate, String user) {
        final String placeId = contentValues.getAsString(PlaceWithRateEntry.COLUMN_NAME_ENTRY_ID);
        LatLng location = new LatLng(contentValues.getAsDouble(PlaceWithRateEntry.COLUMN_NAME_LAT),
                contentValues.getAsDouble(PlaceWithRateEntry.COLUMN_NAME_LNG));
        Intent setRateIntent = RemoteDataService
                .getSetRateIntent(placeId,location,user,rate,this);
        startService(setRateIntent);
    }

    public void openDetails(ContentValues cv) {
        DialogFragment newFragment = DetailFragment.create(cv);
        newFragment.show(getSupportFragmentManager(), "details");
    }

    private void deliverLocationChange(LatLng location) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
        if (fragment!=null && fragment instanceof TargetLocationListener) {
            ((TargetLocationListener) fragment).onLocationChanged(location);
        }
    }

}
