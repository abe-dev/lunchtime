package com.abedev.lunchtime.ui.fragment;

import android.Manifest;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;
import com.abedev.lunchtime.ui.DataLoaderDelegate;
import com.abedev.lunchtime.ui.activity.MainActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by home on 12.09.2016.
 */
public class PlacesMapFragment extends SupportMapFragment implements OnMapReadyCallback, MainActivity.TargetLocationListener, GoogleMap.OnMarkerClickListener {
    public static final int ZOOM = 12;
    public static final int MIN_ZOOM = 10;
    private static final String TAG = PlacesMapFragment.class.getSimpleName();
    private GoogleMap mMap = null;
    private GoogleMap.OnCameraIdleListener mCameraIdleListener;
    private DataLoaderDelegate mLoaderDelegate;
    ArrayMap<String, Marker> markers = new ArrayMap<>();

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.d(TAG, "onCreate");
        mCameraIdleListener = new OnCameraIdle();
        mLoaderDelegate = getDataLoaderDelegate();
        getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady");
        this.mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);;
        }
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.clear();
        mMap.setOnMarkerClickListener(this);
        googleMap.setOnCameraIdleListener(mCameraIdleListener);
    }


    @Override
    public void onLocationChanged(@NonNull LatLng location) {
        if (mMap != null)
            mMap.moveCamera(CameraUpdateFactory
                    .newLatLngZoom(location, ZOOM));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        ((MainActivity) getActivity()).openDetails((ContentValues) marker.getTag());
        return true;
    }

    private class OnCameraIdle implements GoogleMap.OnCameraIdleListener {
        @Override
        public void onCameraIdle() {
            Log.d(TAG,"onCameraIdle");
            if (mMap != null) {
                LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
                mLoaderDelegate.requestDataByArea(bounds.southwest, bounds.northeast);
            }
        }
    }

    @NonNull
    private DataLoaderDelegate getDataLoaderDelegate() {
        return new DataLoaderDelegate(getContext(), getLoaderManager()) {
            @Override
            protected void onDataLoaded(Cursor data) {
               Log.d(TAG,"onDataLoaded");
               if (data != null) {
                    Integer highRate = null;
                    while (data.moveToNext()) {
                        ContentValues cv = PlaceWithRateEntry.asContentValues(data);
                        int rate = cv.getAsInteger(PlaceWithRateEntry.COLUMN_NAME_RATE);
                        if (highRate == null) {
                            highRate = rate;
                        }
                        float alpha;
                        if (rate == highRate)
                            alpha = 1;
                        else
                            alpha = 0.5f;
                        String id = cv.getAsString(PlaceWithRateEntry.COLUMN_NAME_ENTRY_ID);

                        Marker marker;
                        if (markers.containsKey(id)) {
                            marker = markers.get(id);
                            marker.setAlpha(alpha);
                        }
                        else {
                            MarkerOptions options = new MarkerOptions();
                            options.position(new LatLng(cv.getAsDouble(PlaceWithRateEntry.COLUMN_NAME_LAT)
                                    , cv.getAsDouble(PlaceWithRateEntry.COLUMN_NAME_LNG)));
                            options.title(cv.getAsString(PlaceWithRateEntry.COLUMN_NAME_TITLE));
                            options.alpha(alpha);
                            marker = mMap.addMarker(options);
                            markers.put(id,marker);
                        }
                        marker.setTag(cv);
                    }
                }
            }
        };
    }
}
