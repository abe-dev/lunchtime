package com.abedev.lunchtime.ui;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.abedev.lunchtime.R;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;

/**
 * Created by biryuk on 05.10.2016.
 */

public class PlacesRecyclerViewAdapter extends RecyclerView.Adapter<PlacesRecyclerViewAdapter.ViewHolder> implements View.OnClickListener{

    private static final String TAG = PlacesRecyclerViewAdapter.class.getSimpleName();

    public interface AdapterCallback {
        void loadMore();
        void showDetails(ContentValues cv);
    }
    private LayoutInflater mInflater;
    private Cursor mCursor = null;
    private AdapterCallback mCallback;
    private String mPattern;


    public PlacesRecyclerViewAdapter(Context context, LayoutInflater mInflater, AdapterCallback callback) {
        this.mInflater = mInflater;
        mCallback = callback;
        mPattern = context.getString(R.string.pattern_distance);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_place,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view,mPattern);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (mCursor!=null) {
            mCursor.moveToPosition(position);
            holder.bind(mCursor,position);
        }
        if (position == mCursor.getCount()-1) {
            Log.d(TAG,"onBindViewHolder position = " + position);
            mCallback.loadMore();
        }
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        if (mCursor!=null) {
            mCursor.moveToPosition(position);
            ContentValues cv = PlaceWithRateEntry.asContentValues(mCursor);
            mCallback.showDetails(cv);
        }
    }

    @Override
    public int getItemCount() {
        if (mCursor == null)
            return 0;
        else
            return mCursor.getCount();
    }

    public void swapCursor(Cursor cursor) {
        if (cursor != mCursor) {
            if (mCursor != null && !mCursor.isClosed())
                mCursor.close();
            mCursor = cursor;
            notifyDataSetChanged();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private TextView tvAddress;
        private TextView tvDistance;
        private RatingBar rate;
        private String mPattern;
        public ViewHolder(View itemView, String distancePattern) {
            super(itemView);
            mPattern = distancePattern;
            tvTitle = (TextView) itemView.findViewById(R.id.text_title);
            tvDistance = (TextView) itemView.findViewById(R.id.text_distance);
            tvAddress = (TextView) itemView.findViewById(R.id.text_address);
            rate = (RatingBar) itemView.findViewById(R.id.rate);

        }

        public void bind(Cursor cursor, Integer position) {
            itemView.setTag(position);
            tvTitle.setText(cursor.getString(cursor.getColumnIndex(
                    PlaceWithRateEntry.COLUMN_NAME_TITLE)));
            int distance = (int)cursor.getDouble(cursor.getColumnIndex(
                    PlaceWithRateEntry.COLUMN_NAME_DISTANCE));
            tvDistance.setText(String.format(mPattern,distance));
            tvAddress.setText(cursor.getString(cursor.getColumnIndex(
                    PlaceWithRateEntry.COLUMN_NAME_ADDRESS)));
            rate.setProgress(cursor.getInt(cursor.getColumnIndex(
                    PlaceWithRateEntry.COLUMN_NAME_RATE)));
        }
    }
}

