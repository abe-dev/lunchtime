package com.abedev.lunchtime.ui.fragment;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.abedev.lunchtime.R;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;

/**
 * Created by biryuk on 06.10.2016.
 */

public class DetailFragment extends DialogFragment {

    private RatingBar bar;
    private EditText etUser;

    public interface SetRateListener {
        void onRateSet(ContentValues contentValues, int rate, String user);
    }
    private static final String ARG_CONTENT_VALUES = "CV";
    private SetRateListener mListener;
    private View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.fragment_detail, null);
        ContentValues cv = getArguments().getParcelable(ARG_CONTENT_VALUES);
        bar = (RatingBar) view.findViewById(R.id.rate);
        etUser = (EditText) view.findViewById(R.id.et_user);
        String userName = cv.getAsString(PlaceWithRateEntry.COLUMN_NAME_USER);
        etUser.setText(userName);

        TextView tvTitle = (TextView) view.findViewById(R.id.text_title);
        tvTitle.setText(cv.getAsString(PlaceWithRateEntry.COLUMN_NAME_TITLE));
        TextView tvAddress = (TextView) view.findViewById(R.id.text_address);
        tvAddress.setText(cv.getAsString(PlaceWithRateEntry.COLUMN_NAME_ADDRESS));

        if (savedInstanceState == null)
            bar.setProgress(cv.getAsInteger(PlaceWithRateEntry.COLUMN_NAME_RATE));
        else
            bar.setProgress(savedInstanceState.getInt(PlaceWithRateEntry.COLUMN_NAME_RATE));
        builder.setView(view)
                .setPositiveButton(getString(R.string.btn_rate), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        rate();
                    }
                })
                .setNegativeButton(getString(R.string.btn_close), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DetailFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    private void rate() {
        ContentValues cv = getArguments().getParcelable(ARG_CONTENT_VALUES);
        mListener.onRateSet(cv,(int)bar.getRating(),etUser.getText().toString());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        RatingBar bar = (RatingBar) view.findViewById(R.id.rate);
        outState.putInt(PlaceWithRateEntry.COLUMN_NAME_RATE,bar.getProgress());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (SetRateListener) getActivity();
    }

    public static DetailFragment create(ContentValues cv) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_CONTENT_VALUES,cv);
        DetailFragment instance = new DetailFragment();
        instance.setArguments(args);
        return instance;
    }

}
