package com.abedev.lunchtime.ui;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;

import com.abedev.lunchtime.Constants;
import com.abedev.lunchtime.Utils;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;
import com.abedev.lunchtime.service.RemoteDataService;
import com.firebase.geofire.util.GeoUtils;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by home on 05.10.2016.
 */

public abstract class DataLoaderDelegate implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = DataLoaderDelegate.class.getSimpleName();
    private static final int PLACES_LOADER_BY_AREA = 1;
    private static final int PLACES_LOADER_BY_LOCATION = 2;
    private static final String KEY_SW = "SW";
    private static final String KEY_NE = "NE";
    private static final String KEY_LOCATION = "LOCATION";
    private static final String KEY_RADIUS = "RADIUS";
    private Context mContext;
    private LoaderManager mLoaderManager;

    public DataLoaderDelegate(Context context, LoaderManager loaderManager) {
        mContext = context;
        mLoaderManager = loaderManager;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG,"onCreateLoader");
        Loader<Cursor> loader;
        switch (id) {
            case PLACES_LOADER_BY_AREA:
                LatLng sw = args.getParcelable(KEY_SW);
                LatLng ne = args.getParcelable(KEY_NE);
                loader = new CursorLoader(mContext,
                        PlaceWithRateEntry.CONTENT_PLACE_WITH_RATE_URI,
                        null,
                        PlaceWithRateEntry.SELECTION_BY_AREA,
                        PlaceWithRateEntry.getSelectionByAreaArgs(sw,ne),
                        PlaceWithRateEntry.COLUMN_NAME_RATE + " DESC");
                break;
            case PLACES_LOADER_BY_LOCATION:
                LatLng location = args.getParcelable(KEY_LOCATION);
                double radius = args.getDouble(KEY_RADIUS);
                loader = new CursorLoader(mContext,
                        PlaceWithRateEntry.buildUriWithLocationAndRadius(location,radius),
                        null,
                        null,
                        null,
                        null);
                break;
            default:
                throw new UnsupportedOperationException(String.format("Unknown loader type: &d", id));
        }
        return loader;

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG,"onLoadFinished");
        onDataLoaded(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG,"onLoaderReset");
    }

    public void requestDataByArea(LatLng sw, LatLng ne) {
        Log.d(TAG,"requestDataByLocation");
        mContext.startService(RemoteDataService.getLoadByAreaIntent(sw,ne,mContext));
        Bundle args = new Bundle();
        double distance = GeoUtils.distance(sw.latitude, sw.longitude, ne.latitude, ne.longitude);
        if (distance > Constants.MAX_RADIUS*2 || distance < Constants.MIN_RADIUS*2) {
            LatLng center = Utils.getCenter(sw,ne);
            args.putParcelable(KEY_SW, Utils.getShiftedLatLng(center,-Constants.MAX_RADIUS,-Constants.MAX_RADIUS));
            args.putParcelable(KEY_NE, Utils.getShiftedLatLng(center, Constants.MAX_RADIUS, Constants.MAX_RADIUS));
        }
        else {
            args.putParcelable(KEY_SW, sw);
            args.putParcelable(KEY_NE, ne);
        }
        mLoaderManager.restartLoader(PLACES_LOADER_BY_AREA, args, this);
    }

    public void requestDataByLocation(LatLng location, double radius) {
        Log.d(TAG,"requestDataByLocation");
        mContext.startService(RemoteDataService.getLoadByLocationIntent(location,radius,mContext));
        Bundle args = new Bundle();
        args.putParcelable(KEY_LOCATION, location);
        args.putDouble(KEY_RADIUS, Math.min(Constants.MAX_RADIUS,radius));
        mLoaderManager.restartLoader(PLACES_LOADER_BY_LOCATION, args, this);
    }


    abstract protected void onDataLoaded(Cursor data);

}
