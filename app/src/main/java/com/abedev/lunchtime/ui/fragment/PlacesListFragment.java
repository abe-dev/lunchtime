package com.abedev.lunchtime.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abedev.lunchtime.Constants;
import com.abedev.lunchtime.R;
import com.abedev.lunchtime.service.RemoteDataService;
import com.abedev.lunchtime.ui.DataLoaderDelegate;
import com.abedev.lunchtime.ui.PlacesRecyclerViewAdapter;
import com.abedev.lunchtime.ui.activity.MainActivity;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by home on 12.09.2016.
 */
public class PlacesListFragment extends Fragment implements MainActivity.TargetLocationListener,
        PlacesRecyclerViewAdapter.AdapterCallback{
    private static final String TAG = PlacesListFragment.class.getSimpleName();
    private static final String KEY_CURRENT_DISTANCE = "distance";
    private static final String KEY_LOCATION = "location";
    private static final double INITIAL_DISTANCE = 1000;
    private static final double DISTANCE_STEP = 500;
    private DataLoaderDelegate mLoaderDelegate;
    private PlacesRecyclerViewAdapter mAdaper;
    private double mCurrentDistance;
    private LatLng mLocation = null;
    private boolean mIsLoading;
    private ContentLoadingProgressBar pbLoading;
    private BroadcastReceiver mBroadcastReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mAdaper = new PlacesRecyclerViewAdapter(getContext(),getLayoutInflater(savedInstanceState),this);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mIsLoading = false;
                pbLoading.hide();
            }
        };
        mLoaderDelegate = new DataLoaderDelegate(getContext(),getLoaderManager()) {
            @Override
            protected void onDataLoaded(Cursor data) {
                Log.d(TAG,"onDataLoaded count = " + (data == null ? "null" : String.valueOf(data.getCount())));
                mAdaper.swapCursor(data);
            }
        };
        if (savedInstanceState == null)
            mCurrentDistance = INITIAL_DISTANCE;
        else {
            mCurrentDistance = savedInstanceState.getDouble(KEY_CURRENT_DISTANCE);
            mLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            if (mLocation!=null)
                mLoaderDelegate.requestDataByLocation(mLocation,mCurrentDistance);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver,
                new IntentFilter(RemoteDataService.ACTION_REMOTE_DATA_LOADED));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.list_menu,menu);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(KEY_CURRENT_DISTANCE,mCurrentDistance);
        outState.putParcelable(KEY_LOCATION,mLocation);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        pbLoading = (ContentLoadingProgressBar) view.findViewById(R.id.pb_loading);
        pbLoading.hide();
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdaper);
    }

    @Override
    public void onLocationChanged(LatLng location) {
        Log.d(TAG,"onLocationChanged");
        mLocation = location;
        mCurrentDistance = INITIAL_DISTANCE;
        mLoaderDelegate.requestDataByLocation(mLocation,mCurrentDistance);
        showLoading();
    }


    @Override
    public void loadMore() {
        Log.d(TAG,"loadMore mCurrentDistance="+mCurrentDistance);
        if (!mIsLoading && mLocation!=null && mCurrentDistance < Constants.MAX_RADIUS) {
            mCurrentDistance = Math.min(mCurrentDistance+DISTANCE_STEP, Constants.MAX_RADIUS);
            mLoaderDelegate.requestDataByLocation(mLocation,mCurrentDistance);
            showLoading();
        }
    }

    private void showLoading() {
        mIsLoading = true;
        pbLoading.show();
    }
    @Override
    public void showDetails(ContentValues cv) {
        ((MainActivity)getActivity()).openDetails(cv);
    }
}
