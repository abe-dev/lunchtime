package com.abedev.lunchtime;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by biryuk on 07.10.2016.
 */

public class Utils {
    private static final double EARTH_RADIUS = 6378000;
    public static LatLng getCenter(LatLng sw, LatLng ne) {
        return new LatLng((sw.latitude+ne.latitude)/2,(sw.longitude+ne.longitude)/2);
    }

    public static LatLng getShiftedLatLng(LatLng original, double dX, double dY) {
        double newLatitude  = original.latitude  + (dY / EARTH_RADIUS) * (180 / Math.PI);
        double newLongitude = original.longitude + (dX / EARTH_RADIUS)
                * (180 / Math.PI) / Math.cos(original.latitude  * Math.PI/180);
        return new LatLng(newLatitude,newLongitude);
    }
}
