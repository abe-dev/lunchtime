package com.abedev.lunchtime.data.source.local;

import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceEntry;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.RateEntry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by home on 27.08.2016.
 */
public class PlacesDbHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "Places.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String DOUBLE_TYPE = " REAL";
    private static final String INTEGER_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    public static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String CREATE_PLACE_QUERY =
            "CREATE TABLE " + PlaceEntry.TABLE_NAME + "(" +
                    PlaceEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + PRIMARY_KEY +COMMA_SEP +
                    PlaceEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    PlaceEntry.COLUMN_NAME_PHONE + TEXT_TYPE + COMMA_SEP +
                    PlaceEntry.COLUMN_NAME_ADDRESS + TEXT_TYPE + COMMA_SEP +
                    PlaceEntry.COLUMN_NAME_ZIP + TEXT_TYPE + COMMA_SEP +
                    PlaceEntry.COLUMN_NAME_LAT + DOUBLE_TYPE + COMMA_SEP +
                    PlaceEntry.COLUMN_NAME_LNG + DOUBLE_TYPE +
                    " )";

    private static final String CREATE_RATE_QUERY =
            "CREATE TABLE " + RateEntry.TABLE_NAME + "(" +
                    RateEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + PRIMARY_KEY  + COMMA_SEP +
                    RateEntry.COLUMN_NAME_RATE + INTEGER_TYPE + COMMA_SEP +
                    RateEntry.COLUMN_NAME_USER + TEXT_TYPE +
                    " )";

    private static final String CREATE_PLACE_WITH_RATE_QUERY =
            "CREATE VIEW " + PlaceWithRateEntry.TABLE_NAME + " AS SELECT " +
                    "p." + PlaceEntry.COLUMN_NAME_ENTRY_ID + " AS " + PlaceWithRateEntry.COLUMN_NAME_ENTRY_ID + COMMA_SEP +
                    "p." + PlaceEntry.COLUMN_NAME_TITLE + " AS " + PlaceWithRateEntry.COLUMN_NAME_TITLE + COMMA_SEP +
                    "p." + PlaceEntry.COLUMN_NAME_PHONE + " AS " + PlaceWithRateEntry.COLUMN_NAME_PHONE + COMMA_SEP +
                    "p." + PlaceEntry.COLUMN_NAME_ADDRESS + " AS " + PlaceWithRateEntry.COLUMN_NAME_ADDRESS + COMMA_SEP +
                    "p." + PlaceEntry.COLUMN_NAME_ZIP + " AS " + PlaceWithRateEntry.COLUMN_NAME_ZIP + COMMA_SEP +
                    "p." + PlaceEntry.COLUMN_NAME_LAT + " AS " + PlaceWithRateEntry.COLUMN_NAME_LAT + COMMA_SEP +
                    "p." + PlaceEntry.COLUMN_NAME_LNG + " AS " + PlaceWithRateEntry.COLUMN_NAME_LNG + COMMA_SEP +
                    "IFNULL(r." + RateEntry.COLUMN_NAME_RATE + ",0) AS " + PlaceWithRateEntry.COLUMN_NAME_RATE + COMMA_SEP +
                    "IFNULL(r." + RateEntry.COLUMN_NAME_USER + ",'') AS " + PlaceWithRateEntry.COLUMN_NAME_USER + COMMA_SEP +
                    " 0 AS " + PlaceWithRateEntry.COLUMN_NAME_DISTANCE +
                    " FROM " + PlaceEntry.TABLE_NAME + " AS p " +
                    " LEFT JOIN " + RateEntry.TABLE_NAME + " AS r ON p." + PlaceEntry.COLUMN_NAME_ENTRY_ID
                    + "=r." + RateEntry.COLUMN_NAME_ENTRY_ID;


    public PlacesDbHelper(Context context) {
        super(context, DB_NAME,/*factory*/ null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_PLACE_QUERY);
        sqLiteDatabase.execSQL(CREATE_RATE_QUERY);
        sqLiteDatabase.execSQL(CREATE_PLACE_WITH_RATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
