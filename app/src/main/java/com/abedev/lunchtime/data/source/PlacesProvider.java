package com.abedev.lunchtime.data.source;


import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.util.SparseArray;
import android.util.SparseIntArray;

import com.abedev.lunchtime.Utils;
import com.abedev.lunchtime.data.source.local.PlacesDbHelper;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceEntry;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceWithRateEntry;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.RateEntry;
import com.firebase.geofire.util.GeoUtils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.CONTENT_AUTHORITY;


/**
 * Created by home on 27.08.2016.
 */
public class PlacesProvider extends ContentProvider {
    private static final int PLACE = 100;
    private static final int RATE = 200;
    private static final int PLACE_WITH_RATE = 300;
    private static final int PLACE_WITH_RATE_DISTANCE = 400;
    private static final UriMatcher uriMatcher = buildUriMatcher();
    private PlacesDbHelper dbHelper;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(CONTENT_AUTHORITY, PlaceEntry.TABLE_NAME, PLACE);
        uriMatcher.addURI(CONTENT_AUTHORITY, RateEntry.TABLE_NAME, RATE);
        uriMatcher.addURI(CONTENT_AUTHORITY, PlaceWithRateEntry.TABLE_NAME, PLACE_WITH_RATE);
        uriMatcher.addURI(CONTENT_AUTHORITY,
                PlaceWithRateEntry.TABLE_NAME + "/" + PlaceWithRateEntry.COLUMN_NAME_DISTANCE,
                PLACE_WITH_RATE_DISTANCE);
        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new PlacesDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (uriMatcher.match(uri)) {
            case PLACE_WITH_RATE:
                retCursor = dbHelper.getReadableDatabase().query(
                        PlaceWithRateEntry.TABLE_NAME,
                        PlaceWithRateEntry.COLUMNS,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case PLACE_WITH_RATE_DISTANCE:
                double lat = Location.convert(uri.getQueryParameter(PlaceWithRateEntry.PARAM_LOCATION_LAT));
                double lng = Location.convert(uri.getQueryParameter(PlaceWithRateEntry.PARAM_LOCATION_LNG));
                double radius = Double.parseDouble(uri.getQueryParameter(PlaceWithRateEntry.PARAM_RADIUS));
                LatLng center = new LatLng(lat, lng);
                LatLng sw = Utils.getShiftedLatLng(center, -radius, -radius);
                LatLng ne = Utils.getShiftedLatLng(center, +radius, +radius);

                retCursor = dbHelper.getReadableDatabase().query(
                        PlaceWithRateEntry.TABLE_NAME,
                        PlaceWithRateEntry.COLUMNS,
                        PlaceWithRateEntry.SELECTION_BY_AREA,
                        PlaceWithRateEntry.getSelectionByAreaArgs(sw, ne),
                        null,
                        null,
                        sortOrder
                );

                retCursor = getDistanceOrderedCursor(retCursor, new LatLng(lat, lng), radius);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), PlaceWithRateEntry.CONTENT_PLACE_WITH_RATE_URI);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        final int match = uriMatcher.match(uri);
        switch (match) {
            case PLACE:
                return PlacesPersistenceContract.CONTENT_PLACE_TYPE;
            case RATE:
                return PlacesPersistenceContract.CONTENT_RATE_TYPE;
            case PLACE_WITH_RATE:
                return PlacesPersistenceContract.CONTENT_PLACE_WITH_RATE_TYPE;
            default:
                return "Unknown uri type!";
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final int match = uriMatcher.match(uri);
        long insert;
        switch (match) {
            case PLACE:
                insert = dbHelper.getWritableDatabase()
                        .insertWithOnConflict(PlaceEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                break;
            case RATE:
                insert = dbHelper.getWritableDatabase()
                        .insertWithOnConflict(RateEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                break;
            default:
                throw new UnsupportedOperationException(String.format("Not supported for type %s", getType(uri)));
        }

        if (insert == -1)
            return null;
        else {
            return uri;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not supported");
    }

    private Cursor getDistanceOrderedCursor(Cursor cursor, LatLng basePoint, double maxDistance) {
        final int distanceIndex = cursor.getColumnIndex(PlaceWithRateEntry.COLUMN_NAME_DISTANCE);
        final int latIndex = cursor.getColumnIndex(PlaceWithRateEntry.COLUMN_NAME_LAT);
        final int lngIndex = cursor.getColumnIndex(PlaceWithRateEntry.COLUMN_NAME_LNG);
        final int rateIndex = cursor.getColumnIndex(PlaceWithRateEntry.COLUMN_NAME_LNG);
        List<Object[]> rows = new ArrayList<>();
        MatrixCursor result = new MatrixCursor(cursor.getColumnNames());
        final int columnsCount = cursor.getColumnCount();
        while (cursor.moveToNext()) {
            Object[] row = new Object[columnsCount];
            double lat = cursor.getDouble(latIndex);
            double lng = cursor.getDouble(lngIndex);
            double distance = GeoUtils.distance(basePoint.latitude, basePoint.longitude, lat, lng);
            if (distance <= maxDistance) {
                row[latIndex] = lat;
                row[lngIndex] = lng;
                row[distanceIndex] = distance;
                for (int i = 0; i < columnsCount; i++) {
                    if (i != latIndex && i != lngIndex && i != distanceIndex) {
                        if (i == rateIndex)
                            row[i] = cursor.getInt(i);
                        else
                            row[i] = cursor.getString(i);
                    }
                }
                rows.add(row);
            }
        }

        Collections.sort(rows, new Comparator<Object[]>() {
            @Override
            public int compare(Object[] lhs, Object[] rhs) {
                return Double.compare((double) lhs[distanceIndex], (double) rhs[distanceIndex]);
            }
        });

        for (Object[] row : rows) {
            result.addRow(row);
        }
        return result;
    }

}
