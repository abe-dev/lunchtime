package com.abedev.lunchtime.data.source.remote.foursquare;

import android.content.ContentValues;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import static com.abedev.lunchtime.data.source.remote.foursquare.FourSquareServiceUriHelper.*;

import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.PlaceEntry;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by home on 28.08.2016.
 */
public class FourSquareServiceDataSource {
    private static final String TAG = FourSquareServiceDataSource.class.getSimpleName();
    private OkHttpClient httpClient;

    public FourSquareServiceDataSource(OkHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @NonNull
    public ContentValues getPlaceById(String id) {
        JSONObject response = processRequest(getPlaceUri(id));
        if (response == null)
            return new ContentValues();
        else {
            try {
                return mapVenueToContentValues(response.getJSONObject("response").getJSONObject("venue"));
            } catch (JSONException e) {
                throw new RuntimeException("Error parsing response!", e);
            }
        }
    }

    @NonNull
    public List<ContentValues> findPlaces(LatLng location, double radius) {
        JSONObject response = processRequest(searchPlacesUri(location, radius));
        if (response == null)
            return Collections.emptyList();
        else
            return parseSearchVenuesResponse(response);
    }


    @NonNull
    public List<ContentValues> findPlaces(LatLng sw, LatLng ne) {
        JSONObject response = processRequest(searchPlacesUri(sw, ne));
        if (response == null)
            return Collections.emptyList();
        else
            return parseSearchVenuesResponse(response);
    }

    private List<ContentValues> parseSearchVenuesResponse(JSONObject response) {
        List<ContentValues> result = new ArrayList<>();
        JSONArray venues = null;
        try {
            venues = response.getJSONObject("response").getJSONArray("venues");
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing response!", e);
        }
        if (venues != null)
            for (int i = 0; i < venues.length(); i++) {
                ContentValues cv;
                try {
                    cv = mapVenueToContentValues(venues.getJSONObject(i));
                } catch (JSONException e) {
                    Log.e(TAG, "Error parsing venue!", e);
                    continue;
                }
                result.add(cv);
            }
        return result;
    }

    private ContentValues mapVenueToContentValues(JSONObject venue) throws JSONException {
        JSONObject location = venue.getJSONObject("location");
        JSONObject contact = venue.getJSONObject("contact");
        ContentValues cv = new ContentValues();
        cv.put(PlaceEntry.COLUMN_NAME_ENTRY_ID,venue.getString("id"));
        cv.put(PlaceEntry.COLUMN_NAME_TITLE,venue.getString("name"));
        cv.put(PlaceEntry.COLUMN_NAME_PHONE,contact.has("phone") ? contact.getString("phone") : "");
        cv.put(PlaceEntry.COLUMN_NAME_ADDRESS,location.has("address") ? location.getString("address") : "");
        cv.put(PlaceEntry.COLUMN_NAME_ZIP,location.has("postalCode") ? location.getString("postalCode") : "");
        cv.put(PlaceEntry.COLUMN_NAME_LAT,location.getDouble("lat"));
        cv.put(PlaceEntry.COLUMN_NAME_LNG,location.getDouble("lng"));
        return cv;
    }

    private JSONObject processRequest(Uri uri) {
        Request request = new Request.Builder()
                .url(uri.toString())
                .build();
        String jsonString = null;
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (!response.isSuccessful()) {
                response.close();
            }
            jsonString = response.body().string();
        } catch (IOException e) {
            return null;
        } finally {
            if (response != null)
                response.close();
        }

        JSONObject jsonResponse;
        if (jsonString == null)
            jsonResponse = null;
        else
            try {
                jsonResponse = new JSONObject(jsonString);
            } catch (JSONException e) {
                throw new RuntimeException();
            }
        return jsonResponse;
    }

    public class BoundsTooBigException extends RuntimeException {

    }
}
