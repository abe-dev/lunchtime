package com.abedev.lunchtime.data.source.remote.foursquare;

import android.annotation.SuppressLint;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by home on 28.08.2016.
 */
class FourSquareServiceUriHelper {
    private static final String VENUE_BASE_URL = "https://api.foursquare.com/v2/venues";
    private static final String SEARCH_PATH = "search";
    private static final String CLIENT_ID_PARAM_NAME = "client_id";
    private static final String CLIENT_ID = "APEXIJLM1KOQKFIGRL02521CK2EPMEU0NKGVVNKMYJRSHGQP";
    private static final String CLIENT_SECRET_PARAM_NAME = "client_secret";
    private static final String CLIENT_SECRET = "WIALBSU3C1ZHLHDWMHYS2QPPWODYCK011DOWAAT2VBBIO2DE";
    private static final String CLIENT_VERSION_PARAM_NAME = "v";
    private static final String CLIENT_VERSION = "20160810";
    private static final String CATEGORY_ID_PARAM_NAME = "categoryId";
    private static final String FOOD_CATEGORY_ID = "4d4b7105d754a06374d81259";
    private static final String INTENT_PARAM_NAME = "intent";
    private static final String INTENT_BROWSE = "browse";
    private static final String LOCATION_PARAM_NAME = "ll";
    private static final String SOUTH_WEST_PARAM_NAME = "sw";
    private static final String NORTH_EAST_PARAM_NAME = "ne";
    private static final String RADIUS_PARAM_NAME = "radius";

    private static final Uri BASE_URL;
    private static final Uri SEARCH_URL;

    static {
        BASE_URL = Uri.parse(VENUE_BASE_URL).buildUpon()
                .appendQueryParameter(CLIENT_ID_PARAM_NAME,CLIENT_ID)
                .appendQueryParameter(CLIENT_SECRET_PARAM_NAME,CLIENT_SECRET)
                .appendQueryParameter(CLIENT_VERSION_PARAM_NAME,CLIENT_VERSION)
                .build();

        SEARCH_URL = BASE_URL.buildUpon()
                .appendPath(SEARCH_PATH)
                .appendQueryParameter(CATEGORY_ID_PARAM_NAME,FOOD_CATEGORY_ID)
                .appendQueryParameter(INTENT_PARAM_NAME,INTENT_BROWSE)
                .build();
    }

    public static Uri getPlaceUri(String placeId) {
        return BASE_URL.buildUpon().appendPath(placeId).build();
    }

    public static Uri searchPlacesUri(LatLng latLon, double radius) {
        return SEARCH_URL.buildUpon()
                .appendQueryParameter(LOCATION_PARAM_NAME,encodeLocation(latLon))
                .appendQueryParameter(RADIUS_PARAM_NAME,String.valueOf(radius))
                .build();
    }

    public static Uri searchPlacesUri(LatLng sw, LatLng ne) {
        return SEARCH_URL.buildUpon()
                .appendQueryParameter(SOUTH_WEST_PARAM_NAME,encodeLocation(sw))
                .appendQueryParameter(NORTH_EAST_PARAM_NAME,encodeLocation(ne))
                .build();
    }

    @SuppressLint("DefaultLocale")
    private static String encodeLocation(LatLng location) {
        return String.format("%1$f,%2$f", location.latitude, location.longitude);

    }
}
