package com.abedev.lunchtime.data.source.local;

import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;

import com.abedev.lunchtime.BuildConfig;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by home on 22.08.2016.
 */
public final class PlacesPersistenceContract {
    private PlacesPersistenceContract() {
        throw new UnsupportedOperationException("Forbidden!");
    }

    public static final String CONTENT_AUTHORITY = BuildConfig.APPLICATION_ID;
    public static final String CONTENT_PLACE_TYPE = "vnd.android.cursor.dir/"
            + CONTENT_AUTHORITY + "/" + PlaceEntry.TABLE_NAME;;
    public static final String CONTENT_RATE_TYPE = "vnd.android.cursor.dir/"
            + CONTENT_AUTHORITY + "/" + RateEntry.TABLE_NAME;;
    public static final String CONTENT_PLACE_WITH_RATE_TYPE = "vnd.android.cursor.dir/"
            + CONTENT_AUTHORITY + "/" + PlaceWithRateEntry.TABLE_NAME;;
    private static final String CONTENT_SCHEME = "content://";
    public static final Uri BASE_CONTENT_URI = Uri.parse(CONTENT_SCHEME + CONTENT_AUTHORITY);
    private static final String SEPARATOR = "/";

    public static abstract class PlaceEntry {
        public static final String TABLE_NAME = "place";
        public static final String COLUMN_NAME_ENTRY_ID = "placeId";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_PHONE = "phone";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_ZIP = "zip";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LNG = "lon";
        public static final Uri CONTENT_PLACE_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME).build();
    }

    public static abstract class RateEntry {
        public static final String TABLE_NAME = "rate";
        public static final String COLUMN_NAME_ENTRY_ID = "placeId";
        public static final String COLUMN_NAME_USER = "user";
        public static final String COLUMN_NAME_RATE = "rate";
        public static final Uri CONTENT_RATE_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME).build();
    }

    public static abstract class PlaceWithRateEntry {
        public static final String PARAM_LOCATION_LAT = "LAT";
        public static final String PARAM_LOCATION_LNG = "LNG";
        public static final String PARAM_RADIUS = "radius";
        public static final String TABLE_NAME = "placewithrate";
        public static final String COLUMN_NAME_ENTRY_ID = PlaceEntry.COLUMN_NAME_ENTRY_ID;
        public static final String COLUMN_NAME_TITLE = PlaceEntry.COLUMN_NAME_TITLE;
        public static final String COLUMN_NAME_ADDRESS = PlaceEntry.COLUMN_NAME_ADDRESS;
        public static final String COLUMN_NAME_PHONE = PlaceEntry.COLUMN_NAME_PHONE;
        public static final String COLUMN_NAME_ZIP = PlaceEntry.COLUMN_NAME_ZIP;
        public static final String COLUMN_NAME_LAT = PlaceEntry.COLUMN_NAME_LAT;
        public static final String COLUMN_NAME_LNG = PlaceEntry.COLUMN_NAME_LNG;
        public static final String COLUMN_NAME_USER = RateEntry.COLUMN_NAME_USER;
        public static final String COLUMN_NAME_RATE = RateEntry.COLUMN_NAME_RATE;
        public static final String COLUMN_NAME_DISTANCE = "distance";
        public static final String SELECTION_BY_AREA =
                COLUMN_NAME_LAT
                        + ">= ? AND "
                        + COLUMN_NAME_LAT
                        + "<= ? AND "
                        + COLUMN_NAME_LNG
                        + ">= ? AND "
                        + COLUMN_NAME_LNG
                        + "<= ? ";

        public static final String[] COLUMNS = {
                COLUMN_NAME_ENTRY_ID,
                COLUMN_NAME_TITLE,
                COLUMN_NAME_ADDRESS,
                COLUMN_NAME_PHONE,
                COLUMN_NAME_ZIP,
                COLUMN_NAME_LAT,
                COLUMN_NAME_LNG,
                COLUMN_NAME_USER,
                COLUMN_NAME_RATE,
                COLUMN_NAME_DISTANCE};

        ;
        public static final Uri CONTENT_PLACE_WITH_RATE_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME).build();
        public static final Uri CONTENT_PLACE_WITH_RATE_BY_DISTANCE_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME).appendPath(COLUMN_NAME_DISTANCE).build();

        public static String[] getSelectionByAreaArgs(LatLng sw,LatLng ne) {
            return new String[]{
                    Location.convert(sw.latitude,Location.FORMAT_DEGREES),
                    Location.convert(ne.latitude,Location.FORMAT_DEGREES),
                    Location.convert(sw.longitude,Location.FORMAT_DEGREES),
                    Location.convert(ne.longitude,Location.FORMAT_DEGREES)
            };
        }
        public static ContentValues asContentValues(Cursor cursor) {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_NAME_ENTRY_ID,cursor.getString(cursor.getColumnIndex(COLUMN_NAME_ENTRY_ID)));
            cv.put(COLUMN_NAME_TITLE,cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TITLE)));
            cv.put(COLUMN_NAME_ADDRESS,cursor.getString(cursor.getColumnIndex(COLUMN_NAME_ADDRESS)));
            cv.put(COLUMN_NAME_PHONE,cursor.getString(cursor.getColumnIndex(COLUMN_NAME_PHONE)));
            cv.put(COLUMN_NAME_ZIP,cursor.getString(cursor.getColumnIndex(COLUMN_NAME_ZIP)));
            cv.put(COLUMN_NAME_LAT,cursor.getDouble(cursor.getColumnIndex(COLUMN_NAME_LAT)));
            cv.put(COLUMN_NAME_LNG,cursor.getDouble(cursor.getColumnIndex(COLUMN_NAME_LNG)));
            cv.put(COLUMN_NAME_USER,cursor.getString(cursor.getColumnIndex(COLUMN_NAME_USER)));
            cv.put(COLUMN_NAME_RATE,cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_RATE)));
            cv.put(COLUMN_NAME_DISTANCE,cursor.getDouble(cursor.getColumnIndex(COLUMN_NAME_DISTANCE)));
            return cv;
        }

        public static Uri buildUriWithLocationAndRadius(LatLng location, Double radius) {
            return CONTENT_PLACE_WITH_RATE_BY_DISTANCE_URI.buildUpon()
                    .appendQueryParameter(PARAM_LOCATION_LAT,Location.convert(location.latitude,Location.FORMAT_DEGREES))
                    .appendQueryParameter(PARAM_LOCATION_LNG,Location.convert(location.longitude,Location.FORMAT_DEGREES))
                    .appendQueryParameter(PARAM_RADIUS,String.valueOf(radius))
                    .build();
        }



    }

}
