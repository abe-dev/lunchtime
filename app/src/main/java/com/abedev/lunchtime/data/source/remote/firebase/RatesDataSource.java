package com.abedev.lunchtime.data.source.remote.firebase;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.abedev.lunchtime.Constants;
import com.abedev.lunchtime.Utils;
import com.abedev.lunchtime.data.source.local.PlacesPersistenceContract.RateEntry;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.core.GeoHash;
import com.firebase.geofire.core.GeoHashQuery;
import com.firebase.geofire.util.GeoUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by home on 05.09.2016.
 */
public class RatesDataSource {
    private static final String DATA_PATH = "data";
    public static final String KEY_USER = "u";
    public static final String KEY_RATE = "r";
    public static final String KEY_HASH = "g";
    public static final String KEY_LOCATION = "l";
    private FirebaseDatabase firebaseDatabase;

    public RatesDataSource(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public List<ContentValues> loadRates(LatLng sw, LatLng ne, long timeout) {
        LatLng center = Utils.getCenter(sw, ne);
        double radius = GeoUtils.distance(sw.latitude, sw.longitude, center.latitude, center.longitude);
        List<ContentValues> contentValues;
        if (radius >= Constants.MIN_RADIUS && radius <= Constants.MAX_RADIUS)
            contentValues = QueryExecutor.executeQueries(firebaseDatabase, GeoHashQuery.queriesAtLocation(
                    new GeoLocation(center.latitude, center.longitude), radius), timeout);
        else
            contentValues = Collections.emptyList();
        return contentValues;
    }

    public List<ContentValues> loadRates(LatLng center, Double radius, long timeout) {
        return QueryExecutor.executeQueries(firebaseDatabase,
                GeoHashQuery.queriesAtLocation(mapToGeoLocation(center), radius), timeout);
    }

    @Nullable
    public ContentValues getById(String id, long timeout) {
        final CountDownLatch done = new CountDownLatch(1);
        DatabaseReference ref = firebaseDatabase.getReference(DATA_PATH + "/" + id);
        final ContentValues values = new ContentValues();
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                done.countDown();
                if (dataSnapshot.exists()) {
                    values.put(RateEntry.COLUMN_NAME_ENTRY_ID, dataSnapshot.getKey());
                    values.put(RateEntry.COLUMN_NAME_USER,
                            (String) dataSnapshot.child(KEY_USER).getValue());
                    values.put(RateEntry.COLUMN_NAME_RATE,
                            (Integer) dataSnapshot.child(KEY_RATE).getValue());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                done.countDown();
            }
        });
        try {
            done.await(timeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (done.getCount() == 1)
            throw new RuntimeException("Timeout!");
        if (values.size() == 0)
            return null;
        else
            return values;
    }


    public void storePlaceRate(ContentValues placeRate, LatLng placeLocation) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        String id = placeRate.getAsString(RateEntry.COLUMN_NAME_ENTRY_ID);
        DatabaseReference keyRef = firebaseDatabase.getReference(DATA_PATH + "/" + id);

        GeoLocation location = mapToGeoLocation(placeLocation);
        GeoHash geoHash = new GeoHash(location);
        Map<String, Object> updates = new HashMap<>();
        updates.put(KEY_USER, placeRate.getAsString(RateEntry.COLUMN_NAME_USER));
        updates.put(KEY_RATE, placeRate.getAsInteger(RateEntry.COLUMN_NAME_RATE));
        updates.put(KEY_HASH, geoHash.getGeoHashString());
        updates.put(KEY_LOCATION, Arrays.asList(location.latitude, location.longitude));
        keyRef.setValue(updates, geoHash.getGeoHashString(), new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @NonNull
    private GeoLocation mapToGeoLocation(LatLng placeLocation) {
        return new GeoLocation(placeLocation.latitude, placeLocation.longitude);
    }

    private static class QueryExecutor implements ValueEventListener {
        private final Set<GeoHashQuery> queries;
        private final CountDownLatch countDownLatch;
        private final List<ContentValues> result = new ArrayList<>();
        private final long timeout;
        private FirebaseDatabase firebaseDatabase;

        public QueryExecutor(FirebaseDatabase firebaseDatabase, Set<GeoHashQuery> queries, long timeout) {
            this.queries = queries;
            countDownLatch = new CountDownLatch(queries.size());
            this.timeout = timeout;
            this.firebaseDatabase = firebaseDatabase;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                HashMap<String, HashMap<String, Object>> values
                        = (HashMap<String, HashMap<String, Object>>) dataSnapshot.getValue();
                for (Map.Entry<String, HashMap<String, Object>> node : values.entrySet()) {
                    HashMap<String, Object> value = node.getValue();
                    ContentValues cv = new ContentValues();
                    cv.put(RateEntry.COLUMN_NAME_ENTRY_ID, node.getKey());
                    cv.put(RateEntry.COLUMN_NAME_USER, (String) value.get(KEY_USER));
                    cv.put(RateEntry.COLUMN_NAME_RATE, (long) value.get(KEY_RATE));
                    result.add(cv);
                }
            }
            countDownLatch.countDown();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            countDownLatch.countDown();
        }

        private List<ContentValues> execute() {
            for (GeoHashQuery query : queries) {
                Query firebaseQuery = firebaseDatabase
                        .getReference(DATA_PATH)
                        .orderByChild(KEY_HASH)
                        .startAt(query.getStartValue())
                        .endAt(query.getEndValue());
                firebaseQuery.addListenerForSingleValueEvent(this);
            }
            try {
                countDownLatch.await(timeout, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return result;
        }

        public static List<ContentValues> executeQueries(FirebaseDatabase firebaseDatabase, Set<GeoHashQuery> queries, long timeout) {
            return new QueryExecutor(firebaseDatabase, queries, timeout).execute();
        }
    }

}
