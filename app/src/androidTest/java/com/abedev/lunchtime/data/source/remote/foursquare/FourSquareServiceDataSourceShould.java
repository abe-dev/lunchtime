package com.abedev.lunchtime.data.source.remote.foursquare;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by home on 28.08.2016.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class FourSquareServiceDataSourceShould {
    private FourSquareServiceDataSource dataSource;

    @Before
    public void setUp() {
        dataSource = new FourSquareServiceDataSource(new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Log.d("okhhtp",message);
                    }
                }))
                .build());
    }

//    @Test
//    public void load_place_by_id() {
//        final String placeId = "4e4109961f6e54050be4dcf1";
//        Place place = dataSource.getPlaceById(placeId);
//        assertThat(place.getId(),is(equalTo(placeId)));
//    }
//    @Test
//    public void load_places_by_center_and_radius() {
//        LatLon point = new LatLon(50.431334, 30.515917);
//        List<Place> places = dataSource.findPlaces(point,1000d);
//        assertThat(places, not(anyOf(nullValue(),empty())));
//    }
//    @Test
//    public void load_places_by_sw_ne() {
//        LatLon sw = new LatLon(50.429988, 30.514426);
//        LatLon ne = new LatLon(50.432141, 30.517355);
//        List<Place> places = dataSource.findPlaces(sw,ne);
//        assertThat(places, not(anyOf(nullValue(),empty())));
//    }
}
