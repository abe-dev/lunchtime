package com.abedev.lunchtime.data.source.remote.foursquare;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by home on 28.08.2016.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class FourSquareServiceUriHelperShould {
    private OkHttpClient httpClient;
    @Before
    public void setUp() {
        httpClient = new OkHttpClient();
    }
    @Test
    public void generate_correct_place_uri() {
        final String placeId = "4e4109961f6e54050be4dcf1";
        httpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(FourSquareServiceUriHelper.getPlaceUri(placeId).toString())
                .build();
        try {
            Response response = httpClient.newCall(request).execute();
            response.body().close();
            assertThat(response.isSuccessful(),is(true));
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
}
