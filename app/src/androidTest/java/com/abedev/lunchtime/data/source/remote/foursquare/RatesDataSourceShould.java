package com.abedev.lunchtime.data.source.remote.foursquare;

import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import com.abedev.lunchtime.data.source.remote.firebase.RatesDataSource;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

/**
 * Created by home on 19.09.2016.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class RatesDataSourceShould {
    RatesDataSource ratesDataSource;
    @Before
    public void setUp() {
        ratesDataSource = new RatesDataSource(FirebaseDatabase.getInstance());

    }
//    @Test
//    public void store_place_rate() {
//        ratesDataSource.storePlaceRate(new PlaceRate("testid","testuser","fivestars"),new LatLon(33d,22d));
//    }
//    @Test
//    public void read_rate() {
//        PlaceRate rate = ratesDataSource.getById("testid",5000);
//        System.out.print(rate);
//    }
//
//    @Test
//    public void read_by_location_and_radius() {
//        List<PlaceRate> rate = ratesDataSource.loadRates(new LatLon(33d,22d),1d,50000);
//        System.out.print(rate);
//    }
}
